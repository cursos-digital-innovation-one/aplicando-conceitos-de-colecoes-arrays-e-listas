package one.digitalinnovation.collections

fun main() {
    val joao = Funcionario( "Joao", 2000.0, "CLT" )
    val pedro = Funcionario( "Pedro", 1500.0, "PJ" )
    val maria = Funcionario( "Maria", 4000.0, "CLT" )

    println("--------------- Iteração 1 ----------------------")
    val funcionarios = mutableListOf(joao, maria)
    funcionarios.forEach{ println(it)}

    println("--------------- Iteração 2 ----------------------")
    funcionarios.add(pedro)
    funcionarios.forEach{ println(it)}

    println("--------------- Iteração 3 ----------------------")
    funcionarios.remove(pedro)
    funcionarios.forEach{ println(it)}

    println("--------------- Iteração 4 ----------------------")
    val funcionarioSet = mutableSetOf(joao)
    funcionarioSet.forEach{ println(it)}

    println("--------------- Iteração 5 ----------------------")
    funcionarioSet.add(pedro)
    funcionarioSet.add(maria)
    funcionarioSet.forEach{ println(it)}

    println("--------------- Iteração 6 ----------------------")
    funcionarioSet.remove(maria)
    funcionarioSet.forEach{ println(it)}

}