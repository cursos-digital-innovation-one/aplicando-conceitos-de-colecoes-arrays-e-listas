package one.digitalinnovation.collections

fun main() {
    val values = IntArray(5)

    values[0] = 1
    values[1] = 7
    values[2] = 6
    values[3] = 3
    values[4] = 2

    // Iteração 1
    println("Iterando com for")
    for(valor in values) {
        println(valor)
    }
    println()

    // Iteração 2
    println("Iterando com forEach e it")
    values.forEach {
        println(it)
    }
    println()

    // Iteração 3
    println("Iterando com forEach e valor")
    values.forEach { valor ->
        println(valor)
    }
    println()

    // Iteração 4
    println("Iterando com for e index")
    for(index in values.indices) {
        println(values[index])
    }
    println()

    // Iteração 5
    println("Iterando com sort")
    values.sort() // ordena do menor para o maior
    for(valor in values) {
        println(valor)
    }
    println()
}