package one.digitalinnovation.collections

fun main() {
    // Piar é um objeto de pares onde se informa os tipos e depois atribuise os valores
   val pair: Pair<String, Double> = Pair("João", 1000.0)
   val map1= mapOf(pair)

   println("--------------- Map Iteração 1 ----------------------")
   map1.forEach{ (k, v) -> println("chave: $k - valor: $v")}

   val map2 = mapOf(
       "Pedro" to 2500.00,
       "Maria" to 3000.0
   )
    println("--------------- Map Iteração 2 ----------------------")
    map2.forEach{ (k, v) -> println("chave: $k - valor: $v")}
}