package one.digitalinnovation.collections

fun main() {
    val joao = Funcionario( "Joao", 2000.0, "CLT" )
    val pedro = Funcionario( "Pedro", 1500.0, "PJ" )
    val maria = Funcionario( "Maria", 4000.0, "CLT" )

    val funcionarios1 = setOf(joao, pedro)
    val funcionarios2 = setOf( maria)

    println("---------------- Union ---------------------")
    // Uniao dos conjuntos
    val resultUnion = funcionarios1.union(funcionarios2)
    resultUnion.forEach{ println(it)}

    println("--------------- Subtract -------------------")
    // Subtract retira o valor comum entre os dois conjuntos e apresenta o restante
    val funcionarios3 = setOf(joao, pedro, maria)
    val resultSubtract = funcionarios3.subtract(funcionarios2)
    resultSubtract.forEach{ println(it)}

    println("--------------- Intersect ------------------")
    // Intersect retorna o que tem de comum nos dois conjuntos
    val funcionarios4 = setOf(joao, pedro, maria)
    val resultIntersect = funcionarios4.intersect(funcionarios2)
    resultIntersect.forEach{ println(it)}

}